from flask import Flask, render_template, request, redirect, session, flash
from flask_sqlalchemy import SQLAlchemy
from time import sleep
from flask_login import LoginManager
import datetime
import pytz
import random
from flask_admin import Admin
from flask_admin.contrib.sqla import ModelView


app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///offers.db'
db = SQLAlchemy(app)
app.secret_key = "ciwuceidfghe"
app.static_folder = 'static'
USER = "admin"
PASS = "admin"


class offers(db.Model):
    _id = db.Column("id", db.Integer, primary_key=True)
    name = db.Column(db.String(100))
    category = db.Column(db.String(100))
    discount = db.Column(db.String(300))
    aff_link = db.Column(db.String(300))
    off_link = db.Column(db.String(300))
    img = db.Column(db.String(1000))
    
    def __init__(self,name,category,discount,aff_link,off_link,img):
        self.name = name 
        self.category = category
        self.discount = discount
        self.aff_link = aff_link
        self.off_link = off_link
        self.img = img



class ModelSecView(ModelView):
    def is_accessible(self):
        
        if "logged" in session:
           return True

        else:
            
            return False
            


admin = Admin(app)
admin.add_view(ModelSecView(offers, db.session))


@app.route("/admin")
def admin():

    if "logged" in session:
        return render_template("/admin/index.html")
    else:
        return redirect("/login")



@app.route("/login", methods=["GET", "POST"])
def login():
    if request.method == "POST":
        user_try = request.form["user"]
        pass_try = request.form["password"]
        if user_try == USER and pass_try == PASS:
            session["logged"] = True
            return redirect("/admin")
        else:
            flash("wrong!")
    return render_template("/login.html")



 
@app.route("/", methods=["GET", "POST"])
def main():
    stores = [ i for i in offers.query.all()]
    stores_dict = {}
    for s in stores:
        stores_dict[s] = {}
        stores_dict[s]["name"] = s.name
        stores_dict[s]["category"] = s.category
        stores_dict[s]["discount"] = s.discount
        stores_dict[s]["img"] = s.img
        stores_dict[s]["off_link"] = s.off_link



    return render_template("index.html",stores=stores_dict)



@app.route("/new_offer", methods=["GET", "POST"])
def add_offer():
    if request.method == "POST":
        name = request.form["name"]
        session["name"] = name
        category = request.form["category"]
        discount = request.form["discount"]
        aff_link = request.form["aff_link"]
        off_link = request.form["off_link"]
        img = request.form["img"]

        offer = offers(name,category,discount,aff_link,off_link,img)
        db.session.add(offer)
        db.session.commit()
        return redirect("/offers/{}".format(name)) 

    return render_template("add_offer.html")

@app.route("/offers/<offer>")
def offer(offer): 
    offer = offers.query.filter_by(name=offer).first()
    name = offer.name
    category = offer.category
    discount = int(offer.discount)  
    off_link = offer.off_link
    img = offer.img
    session["url"] = offer.aff_link
    stores = [ i.name for i in offers.query.all()]
    x = datetime.datetime.now() 
    dis_time = "{} {}.{}.{}".format(x.strftime("%A"),x.strftime("%m"),x.strftime("%d"),x.strftime("%Y"))
    try:
        if session["safe"] == True:
            
            session["now"] = datetime.datetime.now()
            the_range = session["then"] - session["now"]
            the_range_min = round(the_range.total_seconds() / 60)
            session["counter"] = the_range_min          
    except:      
        session["now"] = datetime.datetime.now()
        delta = datetime.timedelta(minutes=36)
        session["then"] = session["now"] + delta
        the_range = session["then"] - session["now"]
        the_range_min = round(the_range.total_seconds() / 60)
        session["counter"] = the_range_min
        session["safe"] = True   

    return render_template("offer_page.html", name=name,discount=discount,category=category,off_link=off_link,img=img,stores=stores,time=dis_time,counter=session["counter"])


  

@app.route("/analyse/")
def analyse():
    p = random.randint(0,100)
    if p < 50:
        url = session["url"] + "&tid=lp1"
        return render_template("redirect.html",url=url)
    else:
        url = session["url"] + "&tid=lp2"
        return render_template("redirect2.html",url=url)



@app.route("/terms/")
def terms():
    return render_template("terms.html")



@app.route("/privacy/")
def privacy():
    return render_template("privacy.html")



@app.route("/impressum/")
def impressum():
    return render_template("privacy.html")




    

        



if __name__ == "__main__":
    
    
    db.create_all()  
    app.run(debug=false)
